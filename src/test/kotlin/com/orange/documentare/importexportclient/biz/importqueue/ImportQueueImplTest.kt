/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.importexportclient.biz.importqueue

import com.orange.documentare.importexportclient.biz.file.Directory
import com.orange.documentare.importexportclient.biz.file.FileAbsolutePath
import com.orange.documentare.importexportclient.biz.file.FileRelativePath
import com.orange.documentare.importexportclient.biz.file.Filename
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File
import java.nio.file.Files
import java.nio.file.Path

class ImportQueueImplTest {

    @TempDir
    lateinit var docboxDir: File

    @BeforeEach
    fun setup() {
        importDir().mkdirs()
        fileVaultDir().mkdirs()
    }

    @Test
    fun `read directory files, subdirs, without hidden files or dirs, and provide vault file names with duplication support`() {
        // given
        copyTestDocBox()
        val importQueue = ImportQueueImpl(importDir(), fileVaultDir())

        // when
        val filesToUpload = importQueue.readAll()

        // then
        then(filesToUpload).hasSize(4)
        filesToUpload.map { File(it.importDirAbsPath.absolutePath) }.forEach {
            then(it.exists()).isTrue()
            then(it.isFile).isTrue()
            then(it.isRooted).isTrue()
            then(it.isHidden).isFalse()
            then(it.isDirectory).isFalse()
        }

        val importRelativePaths = filesToUpload.map { FileRelativePath.build(it.importDirAbsPath, importDir()) }.map { it.relativePath }
        then(importRelativePaths).containsExactlyInAnyOrder(
                "regular-file.txt", "duplicated-file.txt", "subdir/duplicated-file.txt", "subdir/another-file.txt"
        )

        val vaultRelativePaths = filesToUpload.map { it.vaultName.name }
        then(vaultRelativePaths).containsExactlyInAnyOrder(
                "regular-file.txt", "duplicated-file.txt", "duplicated-file___2.txt", "another-file.txt"
        )
    }

    @Test
    fun `imported file is removed from import directory, and import directory is left empty (subdir + hidden dir also removed)`() {
        // given
        val importDirAbsPath = importDir().dirAbsolutePath
        File("$importDirAbsPath/subdir/").mkdir()
        File("$importDirAbsPath/subdir/.hidden-dir").mkdir()
        File("$importDirAbsPath/subdir/.hidden-dir/toto").createNewFile()
        val fileInImportDirAbsPath = "$importDirAbsPath/subdir/titi"
        fileInImportDirAbsPath.let { File(it).createNewFile() }
        val importQueue = ImportQueueImpl(importDir(), fileVaultDir())

        // when
        importQueue.imported(listOf(FileToUpload(FileAbsolutePath(fileInImportDirAbsPath), Filename("titi"))))

        // then
        then(importDir().listFiles()).isEmpty()
    }

    @Test
    fun `imported files are moved to file vault, flatten without their parent hierarchy`() {
        // given
        copyTestDocBox()
        val importQueue = ImportQueueImpl(importDir(), fileVaultDir())

        // when
        val filesAbsPaths = importQueue.readAll()
        importQueue.imported(filesAbsPaths)

        // then
        val fileVaultFiles = fileVaultDir().listFiles()
        then(fileVaultFiles).hasSize(4)
        then(fileVaultFiles.map { it.file().name }).containsExactlyInAnyOrder(
                "another-file.txt",
                "regular-file.txt",
                "duplicated-file.txt",
                "duplicated-file___2.txt"
        )
        fileVaultFiles.forEach { then(it.file().parentFile.absolutePath).isEqualTo(fileVaultDir().dirAbsolutePath) }
    }

    private fun copyTestDocBox() {
        val resourceDir = File(javaClass.getResource("/test-docbox/").file).toPath()
        Files.walk(resourceDir)
                .map(Path::toFile)
                .filter { it.isFile }
                .forEach {
                    val destFileAbsPath = "${importDir()}/${it.absolutePath.removePrefix(resourceDir.toAbsolutePath().toString())}"
                    File(destFileAbsPath).parentFile.mkdirs()
                    Files.copy(it.toPath(), File(destFileAbsPath).toPath())
                }
    }

    private fun importDir() = Directory("${docboxDir.absolutePath}/in")
    private fun fileVaultDir() = Directory("${docboxDir.absolutePath}/.donttouch-file-vault")
}
