/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.importexportclient.biz.importqueue

import com.orange.documentare.importexportclient.biz.file.Directory
import com.orange.documentare.importexportclient.biz.file.FileAbsolutePath
import com.orange.documentare.importexportclient.biz.file.Filename
import com.orange.documentare.importexportclient.biz.upload.UploadStatus
import com.orange.documentare.importexportclient.biz.upload.Uploader
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File
import java.util.*

class ImporterTest {

    data class UploadEvent(val filename: Filename, val date: Date)

    private val uploadRequests = mutableListOf<UploadEvent>()
    private val uploader = object : Uploader {
        override fun upload(fileToUpload: FileToUpload): UploadStatus {
            uploadRequests.add(UploadEvent(fileToUpload.vaultName, Date()))
            return UploadStatus(true)
        }
    }

    private lateinit var importer: Importer

    @TempDir
    lateinit var docbox: File

    private lateinit var docboxDir: Directory
    private lateinit var importDir: Directory
    private lateinit var filesInVaultDir: Directory

    @BeforeEach
    fun setup() {
        docboxDir = Directory(docbox.absolutePath)
        importDir = Directory("${docbox.absolutePath}/in")
        filesInVaultDir = Directory("${docbox.absolutePath}/.donttouch-file-vault")
        importDir.mkdirs()
        filesInVaultDir.mkdirs()
    }

    @Test
    fun `importer read all files in import dir, waiting that no filesystem change occurred since a long period, then upload files, then move imported files to files vault`() {
        // given
        val listener = { _: List<FileToUpload> ->
            if (uploadRequests.size == 2) {
                File("$importDir/c").createNewFile()
            }
            if (uploadRequests.size == 3) {
                importer.stop()
            }
        }
        importer = Importer(importDir, filesInVaultDir, uploader = uploader, importListener = listener)
        val t0 = Date()

        File("${importDir}/a").createNewFile()
        File("${importDir}/subdir").mkdir()
        File("${importDir}/subdir/b").createNewFile()

        // when
        importer.run()

        // then
        then(uploadRequests.map { it.filename.name }).containsExactlyInAnyOrder("a", "b", "c")
        uploadRequests.map {
            then(it.date.time - t0.time).isGreaterThan(3000)
        }

        val importDirFiles = importDir.listFiles()
        then(importDirFiles.isEmpty()).isTrue()
        val vaultFiles = filesInVaultDir.listFiles()
        then(vaultFiles.map { it.file().name }).containsExactlyInAnyOrder("a", "b", "c")

        Thread.sleep(5000)
    }

    @Test
    fun `files are kept in import dir due to upload errors`() {
        // given
        val listener = { _: List<FileToUpload> ->
            if (uploadRequests.size == 2) {
                importer.stop()
            }
        }
        val uploaderWithErrors = object : Uploader {
            override fun upload(fileToUpload: FileToUpload): UploadStatus {
                uploadRequests.add(UploadEvent(fileToUpload.vaultName, Date()))
                return UploadStatus(false)
            }
        }
        File("${importDir}/a").createNewFile()
        File("${importDir}/subdir").mkdir()
        File("${importDir}/subdir/b").createNewFile()

        importer = Importer(importDir, filesInVaultDir, uploader = uploaderWithErrors, importListener = listener)

        // when
        importer.run()

        // then
        then(importDir.listFiles()).hasSize(2)
    }

    @Test
    fun `stop upload and reread import dir if a filesystem event occurred`() {
        // given
        val importedFilesPerLoop = mutableListOf<List<FileToUpload>>()
        var readAllCount = 0
        val files = listOf(listOf("f1"), listOf("f1", "f2"))
        val importQueue = object : ImportQueue {
            override fun readAll(): List<FileToUpload> =
                    if (readAllCount > 1) {
                        importer.stop()
                        emptyList()
                    } else
                        files[readAllCount++].map { FileToUpload(FileAbsolutePath(it), Filename("not used in test")) }

            override fun stop() {
            }

            override fun imported(importedFiles: List<FileToUpload>) {
                importedFilesPerLoop.add(importedFiles)
            }

            override fun aRecentFilesystemChangeOccurred() = readAllCount == 1
        }
        val uploader = object : Uploader {
            override fun upload(fileToUpload: FileToUpload) = UploadStatus(true)
        }

        importer = Importer(docboxDir, filesInVaultDir, uploader, importQueue)

        // when
        importer.run()

        // then
        then(importedFilesPerLoop[0]).isEmpty()
        then(importedFilesPerLoop[1].map { it.importDirAbsPath }).containsExactly(
                FileAbsolutePath("f1"), FileAbsolutePath("f2")
        )
    }
}
