package com.orange.documentare.importexportclient.biz.importqueue

import com.orange.documentare.importexportclient.biz.file.Directoryname
import com.orange.documentare.importexportclient.biz.file.FileAbsolutePath
import com.orange.documentare.importexportclient.biz.file.Filename
import com.orange.documentare.importexportclient.biz.importqueue.FileToUpload.Companion.buildFilesToUpload
import com.orange.documentare.importexportclient.biz.importqueue.FileToUpload.Companion.homonymousDirectoryname
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class FileToUploadTest {

    @Test
    fun `build files to upload, with file name duplication support, even if file has an extension`() {
        // given
        val filesInImportDirAbsPaths = listOf(
                "/dir/toto",
                "/dir/tutu.png",
                "/dir/titi",
                "/dir/subdir/titi",
                "/dir/subdir/tata.txt",
                "/dir/subdir/roro",
                "/dir/subdir/resubdir/titi"
        ).map { FileAbsolutePath(it) }
        val vaultFilesName = listOf("toto", "tata.txt").map { Filename(it) }

        // when
        val filesToUpload = buildFilesToUpload(filesInImportDirAbsPaths, vaultFilesName)

        // then
        then(filesToUpload).hasSize(7)
        then(filesToUpload).containsExactlyInAnyOrder(
                FileToUpload(FileAbsolutePath("/dir/toto"), Filename("toto___2")),
                FileToUpload(FileAbsolutePath("/dir/tutu.png"), Filename("tutu.png")),
                FileToUpload(FileAbsolutePath("/dir/titi"), Filename("titi")),
                FileToUpload(FileAbsolutePath("/dir/subdir/titi"), Filename("titi___2")),
                FileToUpload(FileAbsolutePath("/dir/subdir/tata.txt"), Filename("tata___2.txt")),
                FileToUpload(FileAbsolutePath("/dir/subdir/roro"), Filename("roro")),
                FileToUpload(FileAbsolutePath("/dir/subdir/resubdir/titi"), Filename("titi___3"))
        )
    }

    @Test
    fun `retrieve optional homonymous filename`() {
        then(homonymousDirectoryname(Filename("billet.pdf"))).isNull()
        then(homonymousDirectoryname(Filename("billet___2.pdf"))).isEqualTo(Directoryname("billet.pdf"))
        then(homonymousDirectoryname(Filename("billet___2"))).isEqualTo(Directoryname("billet"))
        then(homonymousDirectoryname(Filename("billet__2"))).isNull()
    }
}