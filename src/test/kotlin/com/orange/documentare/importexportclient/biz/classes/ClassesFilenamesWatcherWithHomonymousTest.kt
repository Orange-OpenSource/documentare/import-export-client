package com.orange.documentare.importexportclient.biz.classes

import com.orange.documentare.importexportclient.biz.file.Directory
import com.orange.documentare.importexportclient.biz.file.Filename
import com.orange.documentare.importexportclient.infra.classes.ClassesFilenamesClient
import net.jodah.concurrentunit.Waiter
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class ClassesFilenamesWatcherWithHomonymousTest {

    private val waiter = Waiter()
    private val classesFilenamesClient = object : ClassesFilenamesClient {
        override fun classesFilenames() = ClassesFilenames(mapOf(
                Pair(Classname("billets de train"), listOf(Filename("billet_tof.pdf"), Filename("billet.pdf"), Filename("billet___2.pdf"), Filename("billet___3.pdf"))),
                Pair(Classname("3 little pigs"), listOf(Filename("riri"), Filename("fifi___2"))),
                Pair(Classname("zzzz_No_Named"), listOf(Filename("titi.png"), Filename("toto___2"), Filename("toto___3")))
        ))
    }
    private val watchListener = { updatedCount: Int ->
        updatedClassesFilenamesCount = updatedCount
        classesFilenamesWatcher.stop()
        waiter.resume()
    }

    @TempDir
    lateinit var docbox: File

    private lateinit var filesInVaultDir: Directory
    private lateinit var classesDir: Directory
    private lateinit var classesFilenamesWatcher: ClassesFilenamesWatcher
    private var updatedClassesFilenamesCount = -1

    @BeforeEach
    fun setup() {
        filesInVaultDir = Directory("${docbox.absolutePath}/.donttouch-file-vault")
        filesInVaultDir.mkdirs()
        classesDir = Directory("${docbox.absolutePath}/classes")
        classesDir.mkdirs()
        addFilesToVault("billet_tof.pdf", "billet.pdf", "billet___2.pdf", "billet___3.pdf", "riri", "fifi___2", "titi.png", "toto___2", "toto___3")
    }

    @Test
    fun `create classes directory hierarchy from scratch`() {
        // when
        classesFilenamesWatcher = ClassesFilenamesWatcher(filesInVaultDir, classesDir, classesFilenamesClient, watchListener, watchPeriodMs = 10)

        // then
        checkClassesDirectory()
        then(updatedClassesFilenamesCount).isEqualTo(9)
    }

    @Test
    fun `remove unknown class homonymous directory`() {
        // given
        val unknownClassHomonymousDir = classesDir.directory("billets de train/homonymous-dir")
        unknownClassHomonymousDir.mkdirs()
        unknownClassHomonymousDir.file("homonymous-file").create()

        // when
        classesFilenamesWatcher = ClassesFilenamesWatcher(filesInVaultDir, classesDir, classesFilenamesClient, watchListener, watchPeriodMs = 10)

        // then
        checkClassesDirectory()
        then(updatedClassesFilenamesCount).isEqualTo(9)
    }

    @Test
    fun `remove unknown file in homonymous directory`() {
        // given
        val homonymousDir = classesDir.directory("billets de train/billet.pdf")
        homonymousDir.mkdirs()
        homonymousDir.file("unknown-homonymous-file").create()

        // when
        classesFilenamesWatcher = ClassesFilenamesWatcher(filesInVaultDir, classesDir, classesFilenamesClient, watchListener, watchPeriodMs = 10)

        // then
        checkClassesDirectory()
        then(updatedClassesFilenamesCount).isEqualTo(9)
    }

    @Test
    fun `do nothing if classes have not changed`() {
        // given
        createClassesDirectory()

        // when
        classesFilenamesWatcher = ClassesFilenamesWatcher(filesInVaultDir, classesDir, classesFilenamesClient, watchListener, watchPeriodMs = 10)

        // then
        checkClassesDirectory()
        then(updatedClassesFilenamesCount).isEqualTo(0)
    }

    @Test
    fun `transform file to homonymous directory, since new homonymous files were added`() {
        // given
        classesDir.directory("billets de train").mkdirs()
        classesDir.file("billets de train/billet.pdf").create()

        // when
        classesFilenamesWatcher = ClassesFilenamesWatcher(filesInVaultDir, classesDir, classesFilenamesClient, watchListener, watchPeriodMs = 10)

        // then
        checkClassesDirectory()
        then(updatedClassesFilenamesCount).isEqualTo(9)
    }

    private fun createClassesDirectory() {
        classesDir.directory("billets de train/billet.pdf").mkdirs()
        classesDir.directory("3 little pigs/fifi").mkdirs()
        classesDir.directory("zzzz_No_Named/toto").mkdirs()

        classesDir.file("billets de train/billet_tof.pdf").create()
        classesDir.file("billets de train/billet.pdf/billet.pdf").create()
        classesDir.file("billets de train/billet.pdf/billet___2.pdf").create()
        classesDir.file("billets de train/billet.pdf/billet___3.pdf").create()
        classesDir.file("3 little pigs/riri").create()
        classesDir.file("3 little pigs/fifi/fifi___2").create()
        classesDir.file("zzzz_No_Named/titi.png").create()
        classesDir.file("zzzz_No_Named/toto/toto___2").create()
        classesDir.file("zzzz_No_Named/toto/toto___3").create()
    }

    private fun checkClassesDirectory() {
        waiter.await(10000L)
        then(classesDir.listFiles()).hasSize(3)

        then(classesDir.directory("billets de train").listFiles()).hasSize(2)
        then(classesDir.file("billets de train/billet_tof.pdf").isFile()).isTrue()
        then(classesDir.directory("billets de train/billet.pdf").listFiles()).hasSize(3)
        then(classesDir.file("billets de train/billet.pdf/billet.pdf").isFile()).isTrue()
        then(classesDir.file("billets de train/billet.pdf/billet___2.pdf").isFile()).isTrue()
        then(classesDir.file("billets de train/billet.pdf/billet___3.pdf").isFile()).isTrue()

        then(classesDir.directory("3 little pigs").listFiles()).hasSize(2)
        then(classesDir.file("3 little pigs/riri").isFile()).isTrue()
        then(classesDir.directory("3 little pigs/fifi").listFiles()).hasSize(1)
        then(classesDir.file("3 little pigs/fifi/fifi___2").isFile()).isTrue()

        then(classesDir.directory("zzzz_No_Named").listFiles()).hasSize(2)
        then(classesDir.file("zzzz_No_Named/titi.png").isFile()).isTrue()
        then(classesDir.directory("zzzz_No_Named/toto").listFiles()).hasSize(2)
        then(classesDir.file("zzzz_No_Named/toto/toto___2").isFile()).isTrue()
        then(classesDir.file("zzzz_No_Named/toto/toto___3").isFile()).isTrue()
    }

    private fun addFilesToVault(vararg files: String) {
        files.forEach { filesInVaultDir.file(it).create() }
    }
}