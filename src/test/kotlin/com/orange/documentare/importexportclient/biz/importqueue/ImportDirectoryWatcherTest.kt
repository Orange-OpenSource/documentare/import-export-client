package com.orange.documentare.importexportclient.biz.importqueue

import com.orange.documentare.importexportclient.biz.file.Directory
import com.orange.documentare.importexportclient.biz.importqueue.ImportDirectoryWatcher
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File
import java.util.*

class ImportDirectoryWatcherTest {

    @TempDir
    lateinit var dir: File

    @Test
    fun `file system date is updated`() {
        // given
        val t0 = Date()
        val watcher = ImportDirectoryWatcher(Directory(dir.absolutePath))

        // when
        // wait for watching thread to be launched...
        Thread.sleep(500)
        File("${dir.absolutePath}/titi").createNewFile()

        // then
        var dt: Long
        do {
            Thread.sleep(200)
            dt = watcher.lastEventDate().time - t0.time
        } while (dt < 200)
        watcher.stop()
        println("dt = $dt")
    }
}
