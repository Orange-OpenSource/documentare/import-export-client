package com.orange.documentare.importexportclient.biz.duplicates

import com.orange.documentare.importexportclient.biz.file.Directory
import com.orange.documentare.importexportclient.biz.file.FileAbsolutePath
import com.orange.documentare.importexportclient.biz.file.FileRelativePath
import com.orange.documentare.importexportclient.biz.file.Filename
import com.orange.documentare.importexportclient.infra.duplicates.BinClient
import net.jodah.concurrentunit.Waiter
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class DuplicatesWatcherTest {
    private val file1InVaultName = "a"
    private val file2InVaultName = "b"

    @TempDir
    lateinit var docbox: File

    private lateinit var duplicatesDir: Directory
    private lateinit var filesInVaultDir: Directory

    @BeforeEach
    fun setup() {
        duplicatesDir = Directory("${docbox.absolutePath}/duplicates")
        filesInVaultDir = Directory("${docbox.absolutePath}/.donttouch-file-vault")
        filesInVaultDir.mkdirs()
        duplicatesDir.mkdirs()
    }

    @Test
    fun `watcher is running and then we can stop it`() {
        // given
        val binClient = object : BinClient {
            override fun binContent(): List<Filename> = emptyList()
        }

        // when
        val duplicatesWatcher = DuplicatesWatcher(Directory(""), Directory(""), binClient, watchPeriodMs = 10)

        // then
        then(duplicatesWatcher.isRunning()).isTrue()

        // when
        duplicatesWatcher.stop()

        // then
        while (duplicatesWatcher.isRunning()) {
            Thread.sleep(10)
        }
    }

    @Test
    fun `move files in Bin to duplicates directory`() {
        // given
        File("$filesInVaultDir/$file1InVaultName").createNewFile()
        File("$filesInVaultDir/$file2InVaultName").createNewFile()
        val binClient = object : BinClient {
            override fun binContent() = listOf(Filename(file1InVaultName), Filename(file2InVaultName))
        }

        // when
        val duplicatesWatcher = DuplicatesWatcher(filesInVaultDir, duplicatesDir, binClient)

        // then
        var duplicatesAbsPath: List<FileAbsolutePath>
        do {
            Thread.sleep(10)
            duplicatesAbsPath = this.duplicatesDir.listFiles()
        } while (duplicatesAbsPath.size < 2)
        duplicatesWatcher.stop()

        then(duplicatesAbsPath).hasSize(2)
        then(duplicatesAbsPath.map { FileRelativePath.build(it, this.duplicatesDir).relativePath })
                .containsExactlyInAnyOrder(file1InVaultName, file2InVaultName)
    }

    @Test
    fun `do nothing and do not crash if file is already present in duplicates directory`() {
        // given
        File("$filesInVaultDir/$file1InVaultName").createNewFile()
        val duplicate = File("$duplicatesDir/$file1InVaultName")
        duplicate.createNewFile()
        val binClient = object : BinClient {
            override fun binContent() = listOf(Filename(file1InVaultName))
        }

        val waiter = Waiter()
        val watchListener: (duplicates: List<Filename>, _: String?) -> Unit = { _: List<Filename>, _: String? -> waiter.resume() }

        // when
        DuplicatesWatcher(filesInVaultDir, duplicatesDir, binClient, watchListener)

        // then
        waiter.await()

        val duplicatesFiles = duplicatesDir.listFiles()
        then(duplicatesFiles).hasSize(1)
        then(duplicatesFiles[0].absolutePath).isEqualTo(duplicate.absolutePath)
    }

    @Test
    fun `an error is displayed if duplicated file is not found in file vault`() {
        // given
        val binClient = object : BinClient {
            override fun binContent() = listOf(Filename(file1InVaultName))
        }

        val waiter = Waiter()
        val watchListener = { duplicates: List<Filename>, errorMessage: String? ->
            then(duplicates).isEmpty()
            then(errorMessage).isEqualTo("Have you removed a file from file vault? File not found: '${docbox.absolutePath}/.donttouch-file-vault/a'")
            waiter.resume()
        }

        // when
        DuplicatesWatcher(filesInVaultDir, duplicatesDir, binClient, watchListener)

        // then
        waiter.await()
    }
}

