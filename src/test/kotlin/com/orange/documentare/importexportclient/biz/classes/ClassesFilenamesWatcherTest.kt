package com.orange.documentare.importexportclient.biz.classes

import com.orange.documentare.importexportclient.biz.file.Directory
import com.orange.documentare.importexportclient.biz.file.Filename
import com.orange.documentare.importexportclient.infra.classes.ClassesFilenamesClient
import net.jodah.concurrentunit.Waiter
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class ClassesFilenamesWatcherTest {

    private val waiter = Waiter()
    private val classesFilenamesClient = object : ClassesFilenamesClient {
        override fun classesFilenames() = ClassesFilenames(mapOf(
                Pair(Classname("billets de train"), listOf(Filename("billet1.pdf"), Filename("billet2.pdf"))),
                Pair(Classname("zzzz_No_Named"), listOf(Filename("titi.png"), Filename("toto"), Filename("tutu")))
        ))
    }
    private val watchListener = { updatedCount: Int ->
        updatedClassesFilenamesCount = updatedCount
        classesFilenamesWatcher.stop()
        waiter.resume()
    }

    @TempDir
    lateinit var docbox: File

    private lateinit var filesInVaultDir: Directory
    private lateinit var classesDir: Directory
    private lateinit var classesFilenamesWatcher: ClassesFilenamesWatcher
    private var updatedClassesFilenamesCount = -1

    @BeforeEach
    fun setup() {
        filesInVaultDir = Directory("${docbox.absolutePath}/.donttouch-file-vault")
        filesInVaultDir.mkdirs()
        classesDir = Directory("${docbox.absolutePath}/classes")
        classesDir.mkdirs()
        addFilesToVault("billet1.pdf", "billet2.pdf", "titi.png", "toto", "tutu")
    }

    @Test
    fun `create classes directory hierarchy from scratch`() {
        // when
        classesFilenamesWatcher = ClassesFilenamesWatcher(filesInVaultDir, classesDir, classesFilenamesClient, watchListener, watchPeriodMs = 10)

        // then
        checkClassesDirectory()
        then(updatedClassesFilenamesCount).isEqualTo(5)
    }

    @Test
    fun `remove unknown class`() {
        // given
        val unknownClassDir = classesDir.directory("unknown class which should be removed")
        unknownClassDir.mkdirs()
        unknownClassDir.file("fifi").create()

        // when
        classesFilenamesWatcher = ClassesFilenamesWatcher(filesInVaultDir, classesDir, classesFilenamesClient, watchListener, watchPeriodMs = 10)

        // then
        checkClassesDirectory()
        then(updatedClassesFilenamesCount).isEqualTo(5)
    }

    @Test
    fun `remove unknown file in class`() {
        // given
        val knownClassDir = classesDir.directory("billets de train")
        knownClassDir.mkdirs()
        knownClassDir.file("fifi").create()

        // when
        classesFilenamesWatcher = ClassesFilenamesWatcher(filesInVaultDir, classesDir, classesFilenamesClient, watchListener, watchPeriodMs = 10)

        // then
        checkClassesDirectory()
        then(updatedClassesFilenamesCount).isEqualTo(5)
    }

    @Test
    fun `do nothing if classes have not changed`() {
        // given
        createClassesDirectory()

        // when
        classesFilenamesWatcher = ClassesFilenamesWatcher(filesInVaultDir, classesDir, classesFilenamesClient, watchListener, watchPeriodMs = 10)

        // then
        checkClassesDirectory()
        then(updatedClassesFilenamesCount).isEqualTo(0)
    }

    @Test
    fun `watcher is running and then we can stop it`() {
        // given
        val watchListener = { _: Int -> }

        // when
        classesFilenamesWatcher = ClassesFilenamesWatcher(filesInVaultDir, classesDir, classesFilenamesClient, watchListener, watchPeriodMs = 10)

        // then
        then(classesFilenamesWatcher.isRunning()).isTrue()

        // when
        classesFilenamesWatcher.stop()

        // then
        while (classesFilenamesWatcher.isRunning()) {
            Thread.sleep(10)
        }
    }

    @Test
    fun `display an error if a file is not present in vault, and do the job for the rest`() {
        // given
        val classesFilenamesClient = object : ClassesFilenamesClient {
            override fun classesFilenames() = ClassesFilenames(mapOf(
                    Pair(Classname("billets de train"), listOf(Filename("billet1.pdf"), Filename("billet2.pdf"), Filename("file-which-is-not-in-vault"))),
                    Pair(Classname("zzzz_No_Named"), listOf(Filename("titi.png"), Filename("toto"), Filename("tutu")))
            ))
        }

        // when
        classesFilenamesWatcher = ClassesFilenamesWatcher(filesInVaultDir, classesDir, classesFilenamesClient, watchListener, watchPeriodMs = 10)

        // then
        checkClassesDirectory()
        then(updatedClassesFilenamesCount).isEqualTo(5)
    }

    @Test
    fun `display an error and do nothing if the api call failed`() {
        // given
        val classesFilenamesClient = object : ClassesFilenamesClient {
            override fun classesFilenames(): ClassesFilenames? = null
        }

        // when
        classesFilenamesWatcher = ClassesFilenamesWatcher(filesInVaultDir, classesDir, classesFilenamesClient, watchListener, watchPeriodMs = 10)

        // then
        waiter.await(10000L)
        then(updatedClassesFilenamesCount).isEqualTo(0)
    }

    private fun createClassesDirectory() {
        classesDir.directory("billets de train").mkdirs()
        classesDir.file("billets de train/billet1.pdf").create()
        classesDir.file("billets de train/billet2.pdf").create()

        classesDir.directory("zzzz_No_Named").mkdirs()
        classesDir.file("zzzz_No_Named/titi.png").create()
        classesDir.file("zzzz_No_Named/toto").create()
        classesDir.file("zzzz_No_Named/tutu").create()
    }

    private fun checkClassesDirectory() {
        waiter.await(10000L)
        then(classesDir.listFiles()).hasSize(2)

        then(classesDir.directory("billets de train").listFiles()).hasSize(2)
        then(classesDir.file("billets de train/billet1.pdf").isFile()).isTrue()
        then(classesDir.file("billets de train/billet2.pdf").isFile()).isTrue()

        then(classesDir.directory("zzzz_No_Named").listFiles()).hasSize(3)
        then(classesDir.file("zzzz_No_Named/titi.png").isFile()).isTrue()
        then(classesDir.file("zzzz_No_Named/toto").isFile()).isTrue()
        then(classesDir.file("zzzz_No_Named/tutu").isFile()).isTrue()
    }

    private fun addFilesToVault(vararg files: String) {
        files.forEach { filesInVaultDir.file(it).create() }
    }
}