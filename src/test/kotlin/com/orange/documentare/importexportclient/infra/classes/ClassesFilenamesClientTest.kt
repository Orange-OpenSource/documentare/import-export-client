/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.importexportclient.infra.classes

import com.orange.documentare.importexportclient.biz.classes.Classname
import com.orange.documentare.importexportclient.biz.file.Filename
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType


class ClassesFilenamesClientTest {

    private val apiKey = "1234"
    private val jsonResponse = """
 {
  "classesFilenames": {
    "billets de train": [
      "billet.pdf",
      "billet___2.pdf"
    ],
    "edf": [
      "facture.pdf"
    ],
    "zzzz_No_Named": [
      "baboon",
      "gorilla"
    ]
  }
}
    """

    @Test
    fun `classes filenames request is formed correctly`() {
        // given
        val server = MockWebServer()
        server.start()
        server.enqueue(MockResponse().setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON).setBody(jsonResponse).setResponseCode(200))
        val baseUrl = server.url("/").toUrl()

        // when
        val classesFilenames = ClassesFilenamesClientImpl(baseUrl, apiKey).classesFilenames()

        // then
        val request = server.takeRequest()
        server.shutdown()
        then(request.path).isEqualTo("/api/classes-filenames")
        then(request.getHeader("Authorization")).isEqualTo(apiKey)
        then(request.method).isEqualTo("GET")

        then(classesFilenames).isNotNull
        classesFilenames?.classesFilenames!!.let {
            then(it).hasSize(3)
            then(it[Classname("billets de train")]).containsExactlyInAnyOrder(Filename("billet.pdf"), Filename("billet___2.pdf"))
            then(it[Classname("edf")]).containsExactlyInAnyOrder(Filename("facture.pdf"))
            then(it[Classname("zzzz_No_Named")]).containsExactlyInAnyOrder(Filename("baboon"), Filename("gorilla"))
        }
    }

    @Test
    fun `classes filenames request raises an exception if an error occurred`() {
        // given
        val server = MockWebServer()
        server.start()
        server.enqueue(MockResponse().setResponseCode(404))
        val baseUrl = server.url("/").toUrl()

        // when
        val classesFilenames = ClassesFilenamesClientImpl(baseUrl, apiKey).classesFilenames()

        // then
        then(classesFilenames).isNull()
    }
}