/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.importexportclient.infra

import io.quarkus.test.junit.QuarkusTest
import org.assertj.core.api.BDDAssertions.then
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import java.io.File
import javax.inject.Inject

@QuarkusTest
class AppLaunchTest {

    @ConfigProperty(name = "docbox")
    lateinit var docbox: String

    @ConfigProperty(name = "fileVaultName")
    lateinit var fileVaultName: String

    @Inject
    lateinit var appLaunch: AppLaunch

    @AfterEach
    fun cleanup() {
        appLaunchToStop = appLaunch
        docboxDir = File(docbox)
    }

    companion object {
        lateinit var appLaunchToStop: AppLaunch
        lateinit var docboxDir: File

        @AfterAll
        fun stop() {
            appLaunchToStop.testPurposeStopWatchers()
            docboxDir.deleteRecursively()
        }
    }

    @Test
    fun `import default directory is 'docbox in'`() {
        // given
        val importDir = File("docbox/in")

        // then
        then(appLaunch.importerState().importDir.dirAbsolutePath).contains("docbox/in")
        then(appLaunch.importerState().importDir.dirAbsolutePath).isEqualTo(importDir.absolutePath)
    }

    @Test
    fun `'docbox' directory hierarchy is created`() {
        then(File(docbox).isDirectory).isTrue()
        then(File("$docbox/in").isDirectory).isTrue()
        then(File("$docbox/classes").isDirectory).isTrue()
        then(File("$docbox/$fileVaultName").isDirectory).isTrue()
        then(File("$docbox/duplicates").isDirectory).isTrue()
    }

    @Test
    fun `app is launched and importer runs`() {
        then(appLaunch.importerState().running).isTrue()
    }

    @Test
    fun `app is launched and duplicates watcher runs`() {
        then(appLaunch.duplicatesWatcherIsRunning()).isTrue()
    }

    @Test
    fun `app is launched and classes watcher runs`() {
        then(appLaunch.classesWatcherIsRunning()).isTrue()
    }
}