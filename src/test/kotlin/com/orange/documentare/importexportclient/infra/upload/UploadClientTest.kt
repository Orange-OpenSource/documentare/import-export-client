/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.importexportclient.infra.upload

import com.orange.documentare.importexportclient.biz.importqueue.FileToUpload
import com.orange.documentare.importexportclient.biz.upload.UploaderImpl
import com.orange.documentare.importexportclient.biz.file.FileAbsolutePath
import com.orange.documentare.importexportclient.biz.file.Filename
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import java.io.File


class UploadClientTest {

    private val apiKey = "1234"

    @Test
    fun `upload request is formed correctly`() {
        // given
        val server = MockWebServer()
        server.start()
        server.enqueue(MockResponse().setResponseCode(200))
        val baseUrl = server.url("").toString()

        // when
        val fileAbsPath = File(javaClass.getResource("/test-docbox/subdir/another-file.txt").file).absolutePath
        val uploadStatus = UploaderImpl(baseUrl, apiKey).upload(FileToUpload(FileAbsolutePath(fileAbsPath), Filename("another-file.txt")))

        // then
        val request = server.takeRequest()
        server.shutdown()
        then(uploadStatus.ok).isTrue()
        then(request.path).isEqualTo("/api/upload")
        then(request.getHeader("Authorization")).isEqualTo(apiKey)
        then(request.getHeader("Content-Type")).contains("multipart/form-data; boundary")
        then(request.getHeader("Content-Type")).contains("charset=UTF-8")
        then(request.method).isEqualTo("POST")
        val body = request.body.readUtf8()
        then(body).contains("Content-Disposition: form-data; name=\"data\"; filename=\"another-file.txt\"")
        then(body).contains("Content-Type: application/octet-stream")
        then(body).contains("another")
    }

    @Test
    fun `upload request indicates an error if it failed with an http error`() {
        // given
        val server = MockWebServer()
        server.start()
        server.enqueue(MockResponse().setResponseCode(500))
        val baseUrl = server.url("").toString()

        // when
        val fileAbsPath = File(javaClass.getResource("/test-docbox/subdir/another-file.txt").file).absolutePath
        val uploadStatus = UploaderImpl(baseUrl, apiKey).upload(FileToUpload(FileAbsolutePath(fileAbsPath), Filename("another-file.txt")))

        // then
        then(uploadStatus.ok).isFalse()
    }

    @Test
    fun `upload request indicates an error if it failed with an exception`() {
        // given
        val baseUrl = "http://tutu.toto/api/upload"

        // when
        val fileAbsPath = File(javaClass.getResource("/test-docbox/subdir/another-file.txt").file).absolutePath
        val uploadStatus = UploaderImpl(baseUrl, apiKey).upload(FileToUpload(FileAbsolutePath(fileAbsPath), Filename("another-file.txt")))

        // then
        then(uploadStatus.ok).isFalse()
    }
}