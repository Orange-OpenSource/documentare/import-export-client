/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.importexportclient.infra.duplicates

import com.orange.documentare.importexportclient.biz.file.Filename
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType


class BinClientTest {

    private val apiKey = "1234"

    private val jsonResponse = """
 [{
    "_id": "d04098c2-8dd6-45ff-a7d9-4a36ae03e368",
    "filename": "IMG_0403.JPG"
  },
  {
    "_id": "4c86ee25-4edf-46e8-b428-d3da879f7778",
    "filename": "subdir/IMG_1384.jpg"
  }]
    """

    @Test
    fun `bin request is formed correctly`() {
        // given
        val server = MockWebServer()
        server.start()
        server.enqueue(MockResponse().setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON).setBody(jsonResponse).setResponseCode(200))
        val baseUrl = server.url("").toUrl()

        // when
        val binFilesAbsPath = BinClientImpl(baseUrl, apiKey).binContent()

        // then
        val request = server.takeRequest()
        server.shutdown()
        then(request.path).isEqualTo("/api/bin")
        then(request.getHeader("Authorization")).isEqualTo(apiKey)
        then(request.method).isEqualTo("GET")

        then(binFilesAbsPath).containsExactlyInAnyOrder(Filename("IMG_0403.JPG"), Filename("subdir/IMG_1384.jpg"))
    }

    @Test
    fun `bin content is empty if a network error occurred`() {
        // given
        val server = MockWebServer()
        server.start()
        server.enqueue(MockResponse().setResponseCode(503))
        val baseUrl = server.url("/api/bin").toUrl()

        // when
        val binFilesAbsPath = BinClientImpl(baseUrl, apiKey).binContent()

        // then
        then(binFilesAbsPath).isEmpty()
        server.shutdown()
    }
}