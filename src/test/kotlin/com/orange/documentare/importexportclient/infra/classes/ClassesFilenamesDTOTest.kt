/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.importexportclient.infra.classes

import com.orange.documentare.importexportclient.biz.file.Filename
import com.orange.documentare.importexportclient.biz.classes.ClassesFilenames
import com.orange.documentare.importexportclient.biz.classes.Classname
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class ClassesFilenamesDTOTest {

    @Test
    fun `convert dto to business object`() {
        // given
        val givenClassesFilenames = mutableMapOf<String, List<String>>()
        givenClassesFilenames.apply {
            this["billets"] = listOf("billet.pdf", "billet___2.pdf")
            this["images"] = listOf("titi.png")
        }
        val dto = ClassesFilenamesDTO().apply { classesFilenames = givenClassesFilenames }

        // when
        val biz: ClassesFilenames = dto.toBusinessObject()

        // then
        val classesFilenames = biz.classesFilenames
        then(classesFilenames).hasSize(2)
        then(classesFilenames[Classname("billets")]).containsExactlyInAnyOrder(Filename("billet.pdf"), Filename("billet___2.pdf"))
        then(classesFilenames[Classname("images")]).containsExactlyInAnyOrder(Filename("titi.png"))
    }
}