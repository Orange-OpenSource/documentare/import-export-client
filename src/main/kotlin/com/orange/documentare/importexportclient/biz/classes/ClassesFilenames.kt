package com.orange.documentare.importexportclient.biz.classes

import com.orange.documentare.importexportclient.biz.file.Filename

data class ClassesFilenames(val classesFilenames: Map<Classname, List<Filename>>)
