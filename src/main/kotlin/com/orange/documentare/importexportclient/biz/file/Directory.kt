package com.orange.documentare.importexportclient.biz.file

import java.io.File
import java.nio.file.Files
import java.nio.file.Path

data class Directory(val dirAbsolutePath: String) {
    override fun toString() = dirAbsolutePath

    fun directory() = File(dirAbsolutePath)

    fun mkdirs() {
        File(dirAbsolutePath).mkdirs()
    }

    fun listFiles(): List<FileAbsolutePath> =
            File(dirAbsolutePath).listFiles()?.map { FileAbsolutePath(it.absolutePath) } ?: emptyList()

    fun deleteHiddenDirectoriesOrFilesRecursively() {
        Files.walk(directory().toPath())
                .map(Path::toFile)
                .filter { it.isHidden }
                .forEach { it.deleteRecursively() }
    }

    fun file(relativePath: String) = FileAbsolutePath("$dirAbsolutePath/$relativePath")
    fun directory(relativePath: String) = Directory("$dirAbsolutePath/$relativePath")
    fun isDirectory() = directory().isDirectory
}