/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.importexportclient.biz.importqueue

import com.orange.documentare.importexportclient.biz.importqueue.FileToUpload

interface ImportQueue {
    fun readAll(): List<FileToUpload>
    fun imported(importedFiles: List<FileToUpload>)
    fun aRecentFilesystemChangeOccurred(): Boolean
    fun stop()
}
