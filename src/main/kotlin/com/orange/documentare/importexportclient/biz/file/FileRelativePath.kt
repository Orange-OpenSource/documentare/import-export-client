package com.orange.documentare.importexportclient.biz.file

data class FileRelativePath(val relativePath: String) {

    override fun toString() = relativePath

    companion object {
        fun build(fileAbsPath: FileAbsolutePath, directory: Directory) = FileRelativePath(
                fileAbsPath.absolutePath
                        .removePrefix(directory.dirAbsolutePath)
                        .removePrefix("/")
        )
    }
}