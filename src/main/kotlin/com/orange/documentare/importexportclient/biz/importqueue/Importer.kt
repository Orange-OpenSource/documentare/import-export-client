/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.importexportclient.biz.importqueue

import com.orange.documentare.importexportclient.biz.file.Directory
import com.orange.documentare.importexportclient.biz.file.FileRelativePath
import com.orange.documentare.importexportclient.biz.upload.UploadStatus
import com.orange.documentare.importexportclient.biz.upload.Uploader
import org.slf4j.LoggerFactory

class Importer(
        private val importDir: Directory,
        private val fileVaultDir: Directory,
        private val uploader: Uploader,
        private val importQueue: ImportQueue = ImportQueueImpl(importDir, fileVaultDir),
        private val importListener: (filesToUpload: List<FileToUpload>) -> Unit = {}) {

    data class State(val importDir: Directory, val running: Boolean) {
        fun run() = State(this.importDir, true)
        fun stop() = State(this.importDir, false)
    }

    private val log = LoggerFactory.getLogger(Importer::class.java)
    private var state = State(importDir, running = false)

    fun state() = state

    fun stop() {
        state = state.stop()
        importQueue.stop()
    }

    fun run() {
        state = state.run()
        while (state.running) {
            importJob()
        }
    }

    private fun importJob() {
        val filesToUpload = importQueue.readAll()
        val imported = mutableListOf<FileToUpload>()
        for (it in filesToUpload) {
            if (importQueue.aRecentFilesystemChangeOccurred()) {
                break
            }
            if (upload(it).ok) {
                imported.add(it)
            }
        }
        importQueue.imported(imported)
        importListener.invoke(imported)
    }

    private fun upload(fileToUpload: FileToUpload): UploadStatus {
        val fileRelativePath = FileRelativePath.build(fileToUpload.importDirAbsPath, importDir)
        log.info("[Upload] file '${fileRelativePath.relativePath}', renamed as '${fileToUpload.vaultName}'")
        return uploader.upload(fileToUpload)
    }
}
