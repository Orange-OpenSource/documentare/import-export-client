package com.orange.documentare.importexportclient.biz.file

import java.io.File
import java.nio.file.Path

data class FileAbsolutePath(val absolutePath: String) {

    override fun toString() = absolutePath

    fun file() = File(absolutePath)
    fun delete() = file().delete()
    fun isFile() = file().isFile
    fun isDirectory() = file().isDirectory
    fun name(): Filename = Filename(file().name)
    fun path(): Path = file().toPath()
    fun create() {
        file().createNewFile()
    }
    fun asDirectory() = Directory(absolutePath)
}