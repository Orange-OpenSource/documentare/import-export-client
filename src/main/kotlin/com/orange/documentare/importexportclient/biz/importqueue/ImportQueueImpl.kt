/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.importexportclient.biz.importqueue

import com.orange.documentare.importexportclient.biz.importqueue.FileToUpload.Companion.buildFilesToUpload
import com.orange.documentare.importexportclient.biz.file.Directory
import com.orange.documentare.importexportclient.biz.file.FileAbsolutePath
import com.orange.documentare.importexportclient.biz.file.Filename
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.util.*


data class ImportQueueImpl(private val importDir: Directory, private val fileVaultDir: Directory) : ImportQueue {

    private val importDirectoryWatcher = ImportDirectoryWatcher(importDir)
    private var stop = false

    init {
        check(this.importDir.directory().isDirectory) { "'$importDir' is not a valid directory" }
    }

    override fun stop() {
        stop = true
    }

    override fun imported(importedFiles: List<FileToUpload>) {
        importedFiles.forEach { moveFileToVault(it) }
        cleanImportDir()
    }

    override fun readAll(): List<FileToUpload> {
        while (!stop) {
            if (noFileSystemEventForAWhile()) {
                cleanImportDir()
                val filesAbsPaths = mutableListOf<FileAbsolutePath>()
                doReadAll(importDir, filesAbsPaths)
                val filesToUpload = buildFilesToUpload(filesAbsPaths)
                if (filesToUpload.isNotEmpty()) {
                    return filesToUpload
                }
            }
            Thread.sleep(LONG_DURATION_MS)
        }
        return emptyList()
    }

    private fun buildFilesToUpload(filesInImportDirAbsPaths: List<FileAbsolutePath>): List<FileToUpload> {
        val vaultFilesAbsPath = fileVaultDir.listFiles().map { Filename(it.file().name) }
        return buildFilesToUpload(filesInImportDirAbsPaths, vaultFilesAbsPath)
    }

    override fun aRecentFilesystemChangeOccurred() = !noFileSystemEventForAWhile()

    private fun cleanImportDir() {
        importDir.deleteHiddenDirectoriesOrFilesRecursively()

        val directoryFile = importDir.directory()
        Files.walk(directoryFile.toPath())
                .map(Path::toFile)
                .filter { it != directoryFile && it.isDirectory || it.isHidden }
                .forEach { it.delete() }
    }


    private fun moveFileToVault(importedFile: FileToUpload) {
        val fileInImportDir = importedFile.importDirAbsPath.file()
        val fileInVault = File("$fileVaultDir/${importedFile.vaultName.name}")
        Files.move(fileInImportDir.toPath(), fileInVault.toPath())
    }

    private fun noFileSystemEventForAWhile() = (Date().time - importDirectoryWatcher.lastEventDate().time) > LONG_DURATION_MS

    private fun doReadAll(dir: Directory, filesAbsPaths: MutableList<FileAbsolutePath>) {
        val files = dir.listFiles()
        files
                .map { it.file() }
                .filter { !it.isHidden }
                .forEach {
                    if (it.isFile) {
                        filesAbsPaths.add(FileAbsolutePath(it.absolutePath))
                    } else if (it.isDirectory) {
                        doReadAll(Directory(it.absolutePath), filesAbsPaths)
                    }
                }
    }

    companion object {
        private const val LONG_DURATION_MS = 3000L
    }
}
