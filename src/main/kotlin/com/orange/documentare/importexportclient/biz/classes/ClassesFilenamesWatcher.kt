package com.orange.documentare.importexportclient.biz.classes

import com.orange.documentare.importexportclient.biz.file.Directory
import com.orange.documentare.importexportclient.biz.file.Directoryname
import com.orange.documentare.importexportclient.biz.file.FileAbsolutePath
import com.orange.documentare.importexportclient.biz.file.Filename
import com.orange.documentare.importexportclient.biz.importqueue.FileToUpload.Companion.homonymousDirectoryname
import com.orange.documentare.importexportclient.infra.PeriodicRunner
import com.orange.documentare.importexportclient.infra.classes.ClassesFilenamesClient
import org.slf4j.LoggerFactory
import java.nio.file.Files

data class ClassesFilenamesWatcher(val filesVaultDir: Directory, val classesDir: Directory, val classesFilenamesClient: ClassesFilenamesClient, val watchListener: (Int) -> Unit = {}, val watchPeriodMs: Long? = null) {

    private val log = LoggerFactory.getLogger(ClassesFilenamesWatcher::class.java)
    private val periodicRunner: PeriodicRunner

    init {
        periodicRunner = PeriodicRunner(Runnable { doWatch() }, watchPeriodMs)
    }

    private fun doWatch() {
        var updatedClassesFilenamesCount = 0
        classesFilenamesClient.classesFilenames()?.classesFilenames?.apply {
            removeUnknownClasses(keys)
            forEach { (classname, filenames) ->
                updatedClassesFilenamesCount += updateClass(classname, filenames)
            }
        }
        watchListener.invoke(updatedClassesFilenamesCount)
    }

    private fun removeUnknownClasses(classnames: Set<Classname>) {
        classesDir.listFiles().forEach {
            if (!classnames.contains(Classname(it.file().name))) {
                it.file().deleteRecursively()
            }
        }
    }

    private fun updateClass(classname: Classname, filenames: List<Filename>): Int {
        val classnameDir = classesDir.directory(classname.name).apply { mkdirs() }

        cleanupClassDirectory(classnameDir, filenames)
        createHomonymousDirectories(classnameDir, filenames)

        var updatedClassesFilenamesCount = 0
        filenames.forEach {
            updatedClassesFilenamesCount += updateClassFile(it, classnameDir, classname)
        }
        return updatedClassesFilenamesCount
    }

    private fun updateClassFile(filename: Filename, classnameDir: Directory, classname: Classname): Int {
        var updateCount = 0
        val classFile = retrieveClassFile(filename, classnameDir)
        if (!classFile.isFile()) {
            val vaultPath = filesVaultDir.file(filename.name).path()
            try {
                log.info("[Class] move file '${classFile.name()}' to class '${classname.name}'")
                Files.copy(vaultPath, classFile.path())
                updateCount++
            } catch (throwable: Throwable) {
                log.info("[Class] Failed to update class '$classname': '$throwable'; maybe we are still uploading files...")
            }
        }
        return updateCount
    }

    private fun retrieveClassFile(filename: Filename, classnameDir: Directory): FileAbsolutePath {
        val homonymousDirectoryname = homonymousDirectoryname(filename)
        return if (homonymousDirectoryname == null) {
            if (classnameDir.directory(filename.name).isDirectory())
                classnameDir.file(filename.name + "/" + filename.name)
            else
                classnameDir.file(filename.name)
        } else {
            classnameDir.file(homonymousDirectoryname.name + "/" + filename.name)
        }
    }


    private fun createHomonymousDirectories(classnameDir: Directory, filenames: List<Filename>) {
        homonymousDirectories(filenames)
                .map { classnameDir.file(it.name) }
                .filterNot { it.isDirectory() }
                .forEach {
                    it.apply {
                        if (isFile()) {
                            delete()
                        }
                        if (!isDirectory()) {
                            log.info("[Class] create homonymous directory '${it.name()}'")
                            asDirectory().mkdirs()
                        }
                    }
                }
    }

    private fun cleanupClassDirectory(classnameDir: Directory, filenames: List<Filename>) {
        val homonymousDirectories = homonymousDirectories(filenames)
        classnameDir.listFiles().forEach {
            if (!filenames.contains(it.name()) && !homonymousDirectories.contains(Directoryname(it.name().name))) {
                it.file().deleteRecursively()
            }
        }
        homonymousDirectories.forEach { directoryName ->
            classnameDir.directory(directoryName.name).listFiles().forEach {
                if (!filenames.contains(it.name())) {
                    it.file().delete()
                }
            }
        }
    }

    private fun homonymousDirectories(filenames: List<Filename>) =
            filenames.mapNotNull { homonymousDirectoryname(it) }

    fun isRunning() = periodicRunner.isRunning()
    fun stop() {
        periodicRunner.stop()
    }
}