package com.orange.documentare.importexportclient.biz.file

import com.sun.nio.file.SensitivityWatchEventModifier
import java.io.IOException
import java.nio.file.*
import java.nio.file.LinkOption.NOFOLLOW_LINKS
import java.nio.file.StandardWatchEventKinds.*
import java.nio.file.WatchEvent.Kind
import java.nio.file.attribute.BasicFileAttributes

class WatchDirectory(dir: Path) {
    private val watcher: WatchService = FileSystems.getDefault().newWatchService()
    private val keys: MutableMap<WatchKey, Path> = HashMap()
    private var stop = false

    init {
        registerDirAndSubdir(dir)
    }

    fun close() {
        stop = true
        this.watcher.close()
    }

    fun listen(listener: (event: WatchEvent<Path>) -> Unit) {
        while (!stop) {
            val key: WatchKey
            try {
                key = watcher.take()
            } catch (x: InterruptedException) {
                return
            }

            val dir = keys[key] ?: continue
            for (event in key.pollEvents()) {
                val kind = event.kind()
                if (kind === OVERFLOW) {
                    continue
                }

                val ev = event as WatchEvent<Path>
                listener.invoke(ev)

                val name = ev.context()
                val child = dir.resolve(name)
                if (kind === ENTRY_CREATE) {
                    try {
                        if (Files.isDirectory(child, NOFOLLOW_LINKS)) {
                            registerDirAndSubdir(child)
                        }
                    } catch (x: IOException) {
                    }
                }
            }

            val valid = key.reset()
            if (!valid) {
                keys.remove(key)
                if (keys.isEmpty()) {
                    break
                }
            }
        }
    }

    private fun registerDirAndSubdir(start: Path) {
        Files.walkFileTree(start, object : SimpleFileVisitor<Path>() {
            override fun preVisitDirectory(dir: Path, attrs: BasicFileAttributes): FileVisitResult {
                val key = dir.register(watcher, arrayOf<Kind<*>>(ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY), SensitivityWatchEventModifier.HIGH)
                keys[key] = dir
                return FileVisitResult.CONTINUE
            }
        })
    }
}