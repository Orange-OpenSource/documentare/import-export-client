package com.orange.documentare.importexportclient.biz.duplicates

import com.orange.documentare.importexportclient.infra.PeriodicRunner
import com.orange.documentare.importexportclient.biz.file.Directory
import com.orange.documentare.importexportclient.biz.file.Filename
import com.orange.documentare.importexportclient.infra.duplicates.BinClient
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Files
import java.nio.file.NoSuchFileException

data class DuplicatesWatcher(val filesVaultDir: Directory, val duplicatesDir: Directory, val binClient: BinClient, val watchListener: (duplicates: List<Filename>, errorMessage: String?) -> Unit = { _: List<Filename>, _: String? -> }, val watchPeriodMs: Long? = null) {

    private val log = LoggerFactory.getLogger(DuplicatesWatcher::class.java)
    private val periodicRunner: PeriodicRunner

    init {
        periodicRunner = PeriodicRunner(Runnable { doWatch() }, watchPeriodMs)
    }

    private fun doWatch() {
        try {
            val duplicates = binClient.binContent()
            duplicates.forEach {
                moveFileToDuplicatesDirectory(it)
            }
            watchListener.invoke(duplicates, null)
        } catch (throwable: Throwable) {
            val errorMessage = formatAndLogErrorMessage(throwable)
            watchListener.invoke(emptyList(), errorMessage)
        }
    }

    private fun moveFileToDuplicatesDirectory(filename: Filename) {
        val fileInVault = File("$filesVaultDir/$filename")
        val duplicateFileAbsPath = "$duplicatesDir/$filename"
        val duplicateFile = File(duplicateFileAbsPath)

        if (!duplicateFile.exists()) {
            Files.copy(
                    fileInVault.toPath(),
                    File(duplicateFileAbsPath).toPath()
            )
            log.info("[Duplicate] move file '$filename' to duplicates")
        }
    }

    private fun formatAndLogErrorMessage(throwable: Throwable): String {
        var errorMessage = "[Duplicate] An error occurred"
        when (throwable) {
            is NoSuchFileException -> {
                errorMessage = "Have you removed a file from file vault? File not found: '${throwable.message}'"
                log.error(errorMessage)
            }
            else -> log.error(errorMessage, throwable)
        }
        return errorMessage
    }

    fun isRunning() = periodicRunner.isRunning()
    fun stop() {
        periodicRunner.stop()
    }
}