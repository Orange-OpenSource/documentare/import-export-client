package com.orange.documentare.importexportclient.biz.file

data class Filename(val name: String) {
    override fun toString() = name
}