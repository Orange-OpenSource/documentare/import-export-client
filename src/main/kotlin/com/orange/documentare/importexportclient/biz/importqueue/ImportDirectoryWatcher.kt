package com.orange.documentare.importexportclient.biz.importqueue

import com.orange.documentare.importexportclient.biz.file.Directory
import com.orange.documentare.importexportclient.biz.file.WatchDirectory
import org.slf4j.LoggerFactory
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.WatchEvent
import java.util.*
import kotlin.concurrent.thread
import java.nio.file.ClosedWatchServiceException as ClosedWatchServiceException1


class ImportDirectoryWatcher(importDir: Directory) {

    private val log = LoggerFactory.getLogger(ImportDirectoryWatcher::class.java)

    private val filesystemWatcher = WatchDirectory(Paths.get(importDir.dirAbsolutePath))
    private var lastEvent = Date()

    init {
        thread {
            try {
                filesystemWatcher.listen(this::onEvent)
            } catch (e: ClosedWatchServiceException1) {
            }
        }
    }

    private fun onEvent(event: WatchEvent<Path>) {
        lastEvent = Date()
        log.debug("${event.kind()}@${event.context()}@${Date().time}ms")
    }

    fun lastEventDate() = lastEvent

    fun stop() {
        filesystemWatcher.close()
    }
}
