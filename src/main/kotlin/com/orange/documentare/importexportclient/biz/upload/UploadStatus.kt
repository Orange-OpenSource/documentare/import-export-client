package com.orange.documentare.importexportclient.biz.upload

data class UploadStatus(val ok: Boolean)
