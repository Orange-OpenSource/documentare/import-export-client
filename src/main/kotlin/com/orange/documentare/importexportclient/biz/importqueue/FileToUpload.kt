package com.orange.documentare.importexportclient.biz.importqueue

import com.orange.documentare.importexportclient.biz.file.Directoryname
import com.orange.documentare.importexportclient.biz.file.FileAbsolutePath
import com.orange.documentare.importexportclient.biz.file.Filename
import java.io.File

data class FileToUpload(val importDirAbsPath: FileAbsolutePath, val vaultName: Filename) {

    companion object {
        private const val HOMONYMOUS_SEPARATOR = "___"

        fun buildFilesToUpload(filesInImportDirAbsPaths: List<FileAbsolutePath>, vaultFilesName: List<Filename>): List<FileToUpload> {
            val filesToUpload = mutableListOf<FileToUpload>()
            val vaultFileNames = vaultFilesName.map { Filename(File(it.name).name) }.toMutableList()
            filesInImportDirAbsPaths.forEach {
                val vaultName = vaultName(it, vaultFileNames)
                filesToUpload.add(FileToUpload(it, vaultName))
                vaultFileNames.add(vaultName)
            }
            return filesToUpload
        }

        private fun vaultName(fileAbsPath: FileAbsolutePath, vaultFileNames: List<Filename>): Filename {
            val file = File(fileAbsPath.absolutePath)
            val extension = if (file.extension.isEmpty()) "" else ".${file.extension}"
            val fileNameWithoutExtension = file.name.removeSuffix(extension)
            var candidateVaultName = Filename(file.name)
            if (vaultFileNames.contains(candidateVaultName)) {
                var count = 2
                do {
                    candidateVaultName = Filename("${fileNameWithoutExtension}$HOMONYMOUS_SEPARATOR$count$extension")
                    count++
                } while (vaultFileNames.contains(candidateVaultName))
            }
            return candidateVaultName
        }

        fun homonymousDirectoryname(filename: Filename): Directoryname? {
            if (filename.name.contains(HOMONYMOUS_SEPARATOR)) {
                val suffix = filename.name.substringAfterLast(HOMONYMOUS_SEPARATOR)
                val suffixWithoutExtension = suffix.substringBeforeLast(".")
                suffixWithoutExtension.toIntOrNull()?.let {
                    var extension = suffix.substringAfterLast(".", "")
                    if (extension.isNotEmpty()) {
                        extension = ".$extension"
                    }
                    return Directoryname(filename.name.substringBefore(HOMONYMOUS_SEPARATOR) + extension)
                }
            }
            return null
        }
    }
}