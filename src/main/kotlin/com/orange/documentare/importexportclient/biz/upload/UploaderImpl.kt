/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.importexportclient.biz.upload

import com.orange.documentare.importexportclient.biz.importqueue.FileToUpload
import com.orange.documentare.importexportclient.infra.upload.UploadClient
import org.slf4j.LoggerFactory

class UploaderImpl(private val uploadUrl: String, apiKey: String) : Uploader {
    private val path = "/api/upload"
    private val uploadClient = UploadClient(uploadUrl + path, apiKey)
    private val log = LoggerFactory.getLogger(UploaderImpl::class.java)

    override fun upload(fileToUpload: FileToUpload): UploadStatus {
        val uploadStatus = uploadClient.upload(fileToUpload)
        if (!uploadStatus.ok) {
            log.error("[Upload] failed to request '$uploadUrl' for file '${fileToUpload.importDirAbsPath}'")
        }
        return uploadStatus
    }
}
