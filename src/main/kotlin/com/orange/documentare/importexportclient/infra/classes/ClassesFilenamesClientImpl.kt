package com.orange.documentare.importexportclient.infra.classes

import com.orange.documentare.importexportclient.biz.classes.ClassesFilenames
import com.orange.documentare.importexportclient.infra.ClientBuilder.clientBuilder
import org.slf4j.LoggerFactory
import java.net.URL

class ClassesFilenamesClientImpl(private val baseUrl: URL, private val apiKey: String) : ClassesFilenamesClient {

    private val api: ClassesFilenamesApi = clientBuilder(baseUrl, ClassesFilenamesApi::class.java)
    private val log = LoggerFactory.getLogger(ClassesFilenamesClientImpl::class.java)

    override fun classesFilenames(): ClassesFilenames? = try {
        api.classesFilenames(apiKey).toBusinessObject()
    } catch (throwable: Throwable) {
        log.error("[Class] failed to request '$baseUrl': '${throwable.message}'")
        null
    }
}
