package com.orange.documentare.importexportclient.infra

import org.eclipse.microprofile.rest.client.RestClientBuilder
import java.net.URL

object ClientBuilder {
    fun <T> clientBuilder(baseUrl: URL, clazz: Class<T>): T {
        return RestClientBuilder.newBuilder()
                .baseUri(baseUrl.toURI())
                .build(clazz)
    }
}