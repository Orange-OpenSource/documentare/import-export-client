/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.importexportclient.infra.upload

import com.orange.documentare.importexportclient.biz.importqueue.FileToUpload
import com.orange.documentare.importexportclient.biz.upload.UploadStatus
import org.apache.http.HttpHeaders
import org.apache.http.HttpResponse
import org.apache.http.client.methods.RequestBuilder
import org.apache.http.entity.ContentType.DEFAULT_BINARY
import org.apache.http.entity.mime.HttpMultipartMode
import org.apache.http.entity.mime.MultipartEntityBuilder
import org.apache.http.impl.client.HttpClients
import org.slf4j.LoggerFactory

data class UploadClient(private val uploadUrl: String, private val apiKey: String, private var restForAWhileDurationMs: Long = 2000L) {

    private val log = LoggerFactory.getLogger(UploadClient::class.java)

    fun upload(fileToUpload: FileToUpload): UploadStatus {
        val file = fileToUpload.importDirAbsPath.file()
        val client = HttpClients.createDefault()

        val data = MultipartEntityBuilder.create()
                .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
                .addBinaryBody("data", file, DEFAULT_BINARY, fileToUpload.vaultName.name)
                .setCharset(Charsets.UTF_8)
                .build()

        val request = RequestBuilder
                .post(uploadUrl)
                .setEntity(data)
                .setHeader(HttpHeaders.AUTHORIZATION, apiKey)
                .build()

        var uploadStatus = UploadStatus(false)
        val responseHandler = { response: HttpResponse ->
            val status = response.statusLine.statusCode
            uploadStatus = if (status < 200 || status > 300) {
                log.error("[Upload] unexpected response status: $status")
                UploadStatus(false)
            } else {
                UploadStatus(true)
            }
        }
        try {
            client.execute(request, responseHandler)
        } catch (e: Exception) {
            log.warn("[Upload] upload failed: ${e.message}")
            Thread.sleep(restForAWhileDurationMs)
        }
        return uploadStatus
    }
}