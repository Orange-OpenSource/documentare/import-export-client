package com.orange.documentare.importexportclient.infra

import kotlin.concurrent.thread

class PeriodicRunner(private val runnable: Runnable, watchPeriodMsOptional: Long?) {
    private val watchPeriodMs: Long
    private var running = false
    private var stop = false

    init {
        watchPeriodMs = watchPeriodMsOptional ?: WATCH_PERIOD_MS
        thread { watch() }
        running = true
    }

    private fun watch() {
        while (!stop) {
            runnable.run()
            Thread.sleep(watchPeriodMs)
        }
        running = false
    }

    fun isRunning() = running
    fun stop() {
        stop = true
    }

    companion object {
        private const val WATCH_PERIOD_MS = 10 * 1000L
    }
}