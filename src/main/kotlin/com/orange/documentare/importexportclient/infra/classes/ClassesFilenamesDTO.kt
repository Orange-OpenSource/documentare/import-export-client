package com.orange.documentare.importexportclient.infra.classes

import com.orange.documentare.importexportclient.biz.classes.ClassesFilenames
import com.orange.documentare.importexportclient.biz.file.Filename
import com.orange.documentare.importexportclient.biz.classes.Classname

class ClassesFilenamesDTO {
    lateinit var classesFilenames: Map<String, List<String>>

    fun toBusinessObject(): ClassesFilenames {
        val biz = mutableMapOf<Classname, List<Filename>>()
        classesFilenames.forEach { (classname, filenames) ->
            biz[Classname(classname)] = filenames.map { Filename(it) }
        }
        return ClassesFilenames(biz)
    }
}
