/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.importexportclient.infra.duplicates

import com.orange.documentare.importexportclient.biz.file.Filename
import com.orange.documentare.importexportclient.infra.ClientBuilder
import org.slf4j.LoggerFactory
import java.net.URL

class BinClientImpl(private val baseUrl: URL, private val apiKey: String) : BinClient {

    private val api: BinApi = ClientBuilder.clientBuilder(baseUrl, BinApi::class.java)
    private val log = LoggerFactory.getLogger(BinClientImpl::class.java)

    override fun binContent() =
            try {
                api.getBinFiles(apiKey).mapNotNull { it.toBusinessObject() }
            } catch (throwable: Throwable) {
                log.error("[Duplicate] failed to request '$baseUrl': '${throwable.message}'")
                emptyList<Filename>()
            }
}