package com.orange.documentare.importexportclient.infra.duplicates

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.HeaderParam
import javax.ws.rs.Path
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType


@RegisterRestClient
interface BinApi {
    @Path("/api/bin")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    fun getBinFiles(@HeaderParam(HttpHeaders.AUTHORIZATION) apiKey: String): Array<BinFileDTO>
}