package com.orange.documentare.importexportclient.infra.duplicates

import com.orange.documentare.importexportclient.biz.file.Filename

class BinFileDTO {
    var filename: String? = null

    fun toBusinessObject(): Filename? = filename?.let {
        return Filename(it)
    }
}
