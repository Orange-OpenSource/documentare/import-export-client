/*
 * Copyright (c) 2019 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.importexportclient.infra

import com.orange.documentare.importexportclient.biz.classes.ClassesFilenamesWatcher
import com.orange.documentare.importexportclient.biz.duplicates.DuplicatesWatcher
import com.orange.documentare.importexportclient.biz.importqueue.Importer
import com.orange.documentare.importexportclient.biz.upload.UploaderImpl
import com.orange.documentare.importexportclient.biz.file.Directory
import com.orange.documentare.importexportclient.infra.duplicates.BinClientImpl
import com.orange.documentare.importexportclient.infra.classes.ClassesFilenamesClientImpl
import io.quarkus.runtime.StartupEvent
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.slf4j.LoggerFactory
import java.io.File
import java.net.URL
import javax.enterprise.event.Observes
import javax.inject.Singleton
import kotlin.concurrent.thread


@Singleton
class AppLaunch {

    private val log = LoggerFactory.getLogger(AppLaunch::class.java)

    @ConfigProperty(name = "docbox")
    lateinit var docbox: String

    @ConfigProperty(name = "fileVaultName")
    lateinit var fileVaultName: String

    @ConfigProperty(name = "apiKey")
    lateinit var apiKey: String

    @ConfigProperty(name = "clustering.ui")
    lateinit var clusteringUIApi: String

    @ConfigProperty(name = "quarkus.log.file.path")
    lateinit var logPath: String

    private lateinit var importer: Importer
    private lateinit var duplicatesWatcher: DuplicatesWatcher
    private lateinit var classesWatcher: ClassesFilenamesWatcher

    fun atAppLaunch(@Observes event: StartupEvent) {
        log.info("Log file is written here: '$logPath'")
        ensureDocBoxDirExists()
        importer = Importer(inputDirectory(), fileVaultDirectory(), UploaderImpl(clusteringUIApi, apiKey))
        duplicatesWatcher = DuplicatesWatcher(fileVaultDirectory(), duplicatesDirectory(), BinClientImpl(URL(clusteringUIApi), apiKey))
        classesWatcher = ClassesFilenamesWatcher(fileVaultDirectory(), classesDirectory(), ClassesFilenamesClientImpl(URL(clusteringUIApi), apiKey))
        thread { importer.run() }
    }

    private fun ensureDocBoxDirExists() {
        ensureDirExists(Directory(docbox))
        ensureDirExists(inputDirectory())
        ensureDirExists(classesDirectory())
        ensureDirExists(fileVaultDirectory())
        ensureDirExists(duplicatesDirectory())
    }

    private fun ensureDirExists(directory: Directory) {
        if (!directory.directory().exists()) {
            log.info("create directory '$directory'")
            directory.mkdirs()
        }
    }

    private fun inputDirectory() = Directory(File("$docbox/in").absolutePath)
    private fun classesDirectory() = Directory(File("$docbox/classes").absolutePath)
    private fun fileVaultDirectory() = Directory(File("$docbox/$fileVaultName").absolutePath)
    private fun duplicatesDirectory() = Directory(File("$docbox/duplicates").absolutePath)

    fun importerState() = importer.state()

    fun duplicatesWatcherIsRunning() = duplicatesWatcher.isRunning()
    fun classesWatcherIsRunning() = classesWatcher.isRunning()

    fun testPurposeStopWatchers() {
        importer.stop()
        duplicatesWatcher.stop()
        classesWatcher.stop()
    }
}