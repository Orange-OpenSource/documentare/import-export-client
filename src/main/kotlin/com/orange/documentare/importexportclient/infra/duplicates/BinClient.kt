package com.orange.documentare.importexportclient.infra.duplicates

import com.orange.documentare.importexportclient.biz.file.Filename

interface BinClient {
    fun binContent(): List<Filename>
}
