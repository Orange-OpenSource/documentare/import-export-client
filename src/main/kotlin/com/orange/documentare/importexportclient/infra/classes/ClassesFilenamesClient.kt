package com.orange.documentare.importexportclient.infra.classes

import com.orange.documentare.importexportclient.biz.classes.ClassesFilenames

interface ClassesFilenamesClient {
    fun classesFilenames(): ClassesFilenames?
}