## Build and run

Build: `./mvnw clean compile verify package`<br/>
Run: `java -jar target/import-export-client-1.0.0-SNAPSHOT-runner.jar`
## Run options

To specify a remote distinct from `localhost:8080`, add the following option: `clustering.ui`<br/>
So for instance: `java -jar -Dclustering.ui=http://g-z440-cm:8080 -jar target/import-export-client-1.0.0-SNAPSHOT-runner.jar`
